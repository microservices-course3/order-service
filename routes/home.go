package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func InitHomeRoute(rg *gin.RouterGroup) {
	r := rg.Group("/")

	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "Order Service v1.0.0",
		})
	})
}
