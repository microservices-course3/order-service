package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/lib/pq"
	"github.com/sing3demons/order-service/middleware"
	"github.com/sing3demons/order-service/model"
	"gorm.io/gorm"
)

type Body struct {
	ProductID pq.Int64Array `json:"product_id"`
}

func InitOrderRoute(rg *gin.RouterGroup, db *gorm.DB) {
	r := rg.Group("/order")
	r.Use(middleware.AuthJWT())

	r.POST("/", func(c *gin.Context) {
		user_id := c.MustGet("user_id")
		var body Body

		if err := c.ShouldBindJSON(&body); err != nil {
			c.JSON(400, gin.H{
				"message": "Bad Request",
			})
			return
		}

		order := model.Order{
			UserID:    uint(user_id.(float64)),
			ProductID: body.ProductID,
		}

		tx := db.Create(&order)

		if tx.Error != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": "Bad Request",
			})
			return
		}

		c.JSON(http.StatusCreated, gin.H{"order": order})
	})

	r.GET("/", func(c *gin.Context) {

		var orders []model.Order
		role := c.MustGet("role").(string)
		if role != "admin" {
			user_id := c.MustGet("user_id")
			tx := db.Where("user_id = ?", user_id).Find(&orders)
			if tx.Error != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"message": "Bad Request",
				})
				return
			}

			c.JSON(http.StatusOK, gin.H{
				"orders": orders,
			})
			return
		}

		tx := db.Find(&orders)

		if tx.Error != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": "Bad Request",
			})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"orders": orders,
		})

	})

	r.GET("/:id", func(ctx *gin.Context) {
		id := ctx.Param("id")
		var order model.Order

		tx := db.First(&order, id)

		if tx.Error != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"message": "Bad Request",
			})
			return
		}

		ctx.JSON(http.StatusOK, gin.H{
			"order": order,
		})
	})
	// r.PUT("/:id", UpdateOrder)
	// r.DELETE("/:id", DeleteOrder)
}
