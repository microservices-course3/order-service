package model

import (
	"github.com/lib/pq"
	"gorm.io/gorm"
)

type Order struct {
	gorm.Model
	UserID    uint          `json:"user_id" gorm:"type:integer;not null"`
	ProductID pq.Int64Array `json:"product_id" gorm:"type:integer[];not null"`
}
