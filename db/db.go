package db

import (
	"fmt"
	"os"

	"github.com/sing3demons/order-service/model"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func ConnectDB() *gorm.DB {
	dsn := os.Getenv("DATABASE_DNS")

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	fmt.Println("Database connected successfully")

	db.AutoMigrate(&model.Order{})

	return db
}
