package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/sing3demons/order-service/db"
	"github.com/sing3demons/order-service/routes"
)

func init() {
	err := godotenv.Load()
	if gin.Mode() != gin.ReleaseMode {
		if err != nil {
			log.Fatal("Error loading .env file")
		}
	}

}

func main() {
	gin.SetMode(os.Getenv("GIN_MODE"))
	db := db.ConnectDB()

	conn, err := amqp.Dial(os.Getenv("RABBITMQ_URL"))
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	ConsumeRabbitMQ(conn)

	router := gin.Default()
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"*"},
		AllowHeaders:     []string{"*"},
		ExposeHeaders:    []string{"*"},
		AllowCredentials: true,
	}))
	apiV1 := router.Group("/api/v1")
	routes.InitHomeRoute(apiV1)
	routes.InitOrderRoute(apiV1, db)
	router.Run(":" + os.Getenv("PORT"))
}

func ConsumeRabbitMQ(conn *amqp.Connection) {
	ch, err := conn.Channel()
	if err != nil {
		log.Fatal(err)
	}
	defer ch.Close()

	msgs, err := ch.Consume("q.sing.order.service", "", true, false, false, false, nil)
	if err != nil {
		log.Fatal(err)
	}

	var forever chan struct{}

	go func() {
		for d := range msgs {
			fmt.Printf("Received a message: %s\n", d.Body)
			// fmt.Printf("Done")
			// d.Ack(true)
		}
	}()

	fmt.Println(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}
